package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"sync"

	"github.com/davecgh/go-spew/spew"
	jsoniter "github.com/json-iterator/go"
)

var tempPath = "/tmp"

type MediaInfo struct {
	Media struct {
		Ref   string `json:"@ref"`
		Track []struct {
			Type                    string `json:"@type"`
			VideoCount              string `json:"VideoCount,omitempty"`
			AudioCount              string `json:"AudioCount,omitempty"`
			FileExtension           string `json:"FileExtension,omitempty"`
			Format                  string `json:"Format"`
			FormatProfile           string `json:"Format_Profile,omitempty"`
			CodecID                 string `json:"CodecID"`
			CodecIDCompatible       string `json:"CodecID_Compatible,omitempty"`
			FileSize                string `json:"FileSize,omitempty"`
			Duration                string `json:"Duration"`
			OverallBitRateMode      string `json:"OverallBitRate_Mode,omitempty"`
			OverallBitRate          string `json:"OverallBitRate,omitempty"`
			FrameRate               string `json:"FrameRate"`
			FrameCount              string `json:"FrameCount"`
			StreamSize              string `json:"StreamSize"`
			HeaderSize              string `json:"HeaderSize,omitempty"`
			DataSize                string `json:"DataSize,omitempty"`
			FooterSize              string `json:"FooterSize,omitempty"`
			IsStreamable            string `json:"IsStreamable,omitempty"`
			EncodedDate             string `json:"Encoded_Date"`
			TaggedDate              string `json:"Tagged_Date"`
			FileModifiedDate        string `json:"File_Modified_Date,omitempty"`
			FileModifiedDateLocal   string `json:"File_Modified_Date_Local,omitempty"`
			StreamOrder             string `json:"StreamOrder,omitempty"`
			ID                      string `json:"ID,omitempty"`
			FormatLevel             string `json:"Format_Level,omitempty"`
			FormatSettingsCABAC     string `json:"Format_Settings_CABAC,omitempty"`
			FormatSettingsRefFrames string `json:"Format_Settings_RefFrames,omitempty"`
			BitRate                 string `json:"BitRate,omitempty"`
			BitRateMaximum          string `json:"BitRate_Maximum,omitempty"`
			Width                   string `json:"Width,omitempty"`
			Height                  string `json:"Height,omitempty"`
			SampledWidth            string `json:"Sampled_Width,omitempty"`
			SampledHeight           string `json:"Sampled_Height,omitempty"`
			PixelAspectRatio        string `json:"PixelAspectRatio,omitempty"`
			DisplayAspectRatio      string `json:"DisplayAspectRatio,omitempty"`
			Rotation                string `json:"Rotation,omitempty"`
			FrameRateMode           string `json:"FrameRate_Mode,omitempty"`
			ColorSpace              string `json:"ColorSpace,omitempty"`
			ChromaSubsampling       string `json:"ChromaSubsampling,omitempty"`
			BitDepth                string `json:"BitDepth,omitempty"`
			ScanType                string `json:"ScanType,omitempty"`
			EncodedLibrary          string `json:"Encoded_Library,omitempty"`
			EncodedLibraryName      string `json:"Encoded_Library_Name,omitempty"`
			EncodedLibraryVersion   string `json:"Encoded_Library_Version,omitempty"`
			EncodedLibrarySettings  string `json:"Encoded_Library_Settings,omitempty"`
			Extra                   struct {
				CodecConfigurationBox string `json:"Codec_configuration_box"`
			} `json:"extra,omitempty"`
			FormatSettingsSBR        string `json:"Format_Settings_SBR,omitempty"`
			FormatAdditionalFeatures string `json:"Format_AdditionalFeatures,omitempty"`
			BitRateMode              string `json:"BitRate_Mode,omitempty"`
			Channels                 string `json:"Channels,omitempty"`
			ChannelPositions         string `json:"ChannelPositions,omitempty"`
			ChannelLayout            string `json:"ChannelLayout,omitempty"`
			SamplesPerFrame          string `json:"SamplesPerFrame,omitempty"`
			SamplingRate             string `json:"SamplingRate,omitempty"`
			SamplingCount            string `json:"SamplingCount,omitempty"`
			CompressionMode          string `json:"Compression_Mode,omitempty"`
			StreamSizeProportion     string `json:"StreamSize_Proportion,omitempty"`
			Language                 string `json:"Language,omitempty"`
		} `json:"track"`
	} `json:"media"`
}

type FileInfo struct {
	SourceNamePath string // the original full name and path of the file
	SourcePath     string // the original path of the file
	SourceName     string // the original file name
	NameTmpPath    string // the temporary location for the origial name and temp path
	TmpPath        string // the temporary file path
	NewName        string // the new file name (no special chars and no spaces)
	NewNameTmpPath string // the new name in the temp file path location
}

func main() {
	fp := flag.String("path", "", "folder or file path required")
	p := flag.String("preset", "medium", "ultrafast superfast veryfast faster fast medium slow slower veryslow placebo\n\thttps://x265.readthedocs.io/en/default/presets.html")
	flag.Parse()

	if *fp == "" {
		log.Println("full file path or folder path is required")
		os.Exit(1)
	}

	fpath := *fp
	preset := *p

	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		fmt.Println("file or folder does not exist:", fpath)
		os.Exit(1)
	}

	TestForApps()

	files, _ := ioutil.ReadDir(fpath)

	if len(files) == 0 {
		fileInfo := ParseFilePath(tempPath, fpath)
		ProcessFile(preset, fileInfo)
		os.Exit(0)
	}

	for _, f := range files {
		// if f is not a directory process the file
		if !f.IsDir() {
			fileInfo := ParseFilePath(tempPath, fpath+"/"+f.Name())
			ProcessFile(preset, fileInfo)
		}
	}

}

// ParseFilePath will take the source full path and file name
// and break it into each of it's path and name parts
// the new file name is appended with .h265.mkv
func ParseFilePath(tmpPath, originalNamePath string) FileInfo {
	p := strings.Split(originalNamePath, "/")
	i := strings.LastIndex(originalNamePath, "/")

	f := FileInfo{
		SourceNamePath: originalNamePath,
		SourcePath:     originalNamePath[:i],
		SourceName:     p[len(p)-1],
		NameTmpPath:    tmpPath + "/" + p[len(p)-1],
		TmpPath:        tmpPath,
	}

	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		log.Fatal(err)
	}

	f.NewName = reg.ReplaceAllString(p[len(p)-1], ".")
	f.NewName = f.NewName + ".h265.mkv"
	f.NewNameTmpPath = f.TmpPath + "/" + f.NewName
	spew.Dump(f)

	return f
}

func ProcessFile(preset string, f FileInfo) {
	mi := GetMediaInfo(f)
	for _, v := range mi.Media.Track {
		if v.Type == "Video" && v.Format != "HEVC" {
			fmt.Println("file format is:", v.Format, "with encoding library", v.EncodedLibrary)
			fmt.Println("processing file", mi.Media.Ref)

			CopyFile(f.SourceNamePath, f.TmpPath)                  // copies the file to a temp location for processing
			ReEncodeToHEVC(preset, f)                              // re-encodes the file and saves to temp path with new name
			CopyFile(f.NewNameTmpPath, f.SourcePath+"/"+f.NewName) // copies the new file back to the origial folder
			RmTempFiles(f)                                         // removes the temp files that were created
			RmFile(f.SourceNamePath)                               // remove the original file
		}
	}
}

// ReEncodeToHEVC uses ffmpeg to re encode the file video stream into HEVC (h265) format
// HEVC has the best compression for high quality HD video files
// audio stream is copied from the original, as re encoding the audio would slow the process even more
// output file is appended with .h265.mkv
func ReEncodeToHEVC(preset string, f FileInfo) {
	if preset == "" {
		preset = "medium"
	}
	// the -y flag will force overwrite the file if it exists (see ffmpeg docs https://ffmpeg.org/ffmpeg.html)
	cmd := exec.Command("ffmpeg", "-i", f.NameTmpPath, "-hide_banner", "-y", "-c:v", "libx265", "-c:a", "copy", "-preset", preset, f.NewNameTmpPath)

	var stdout, stderr []byte
	var errStdout, errStderr error
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		log.Fatalf("cmd.Start() failed with '%s'\n", err)
	}

	// cmd.Wait() should be called only after we finish reading
	// from stdoutIn and stderrIn.
	// wg ensures that we finish
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		stdout, errStdout = copyAndCapture(os.Stdout, stdoutIn)
		wg.Done()
	}()

	stderr, errStderr = copyAndCapture(os.Stderr, stderrIn)

	wg.Wait()

	err = cmd.Wait()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	if errStdout != nil || errStderr != nil {
		log.Fatal("failed to capture stdout or stderr\n")
	}
	outStr, errStr := string(stdout), string(stderr)
	fmt.Printf("\nout:\n%s\nerr:\n%s\n", outStr, errStr)
}

// GetMediaInfo uses the mediainfo program to capture the media file inforamtion
func GetMediaInfo(f FileInfo) MediaInfo {
	var mediaInfo MediaInfo
	cmd := exec.Command("mediainfo", "--Output=JSON", f.SourceNamePath)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	json := jsoniter.ConfigFastest

	err = json.Unmarshal(out.Bytes(), &mediaInfo)
	if err != nil {
		log.Println("error:", err, "cannot unmarshal output: ", out.String())
		log.Fatal(err)
	}

	return mediaInfo
}

// will copy the file source to dest using the rsync program
// error return means the copy didn't work
func CopyFile(source, dest string) {
	fmt.Println("attempt to copy the file from", source, "to", dest)

	cmd := exec.Command("rsync", "--ignore-existing", "-raz", "--progress", source, dest)

	var stdout, stderr []byte
	var errStdout, errStderr error
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		log.Fatalf("cmd.Start() failed with '%s'\n", err)
	}

	// cmd.Wait() should be called only after we finish reading
	// from stdoutIn and stderrIn.
	// wg ensures that we finish
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		stdout, errStdout = copyAndCapture(os.Stdout, stdoutIn)
		wg.Done()
	}()

	stderr, errStderr = copyAndCapture(os.Stderr, stderrIn)

	wg.Wait()

	err = cmd.Wait()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	if errStdout != nil || errStderr != nil {
		log.Fatal("failed to capture stdout or stderr\n")
	}
	outStr, errStr := string(stdout), string(stderr)
	fmt.Printf("\nout:\n%s\nerr:\n%s\n", outStr, errStr)
}

func RmTempFiles(f FileInfo) {
	RmFile(f.NameTmpPath)
	RmFile(f.NewNameTmpPath)
}

// removes (deletes) a file
func RmFile(filePath string) {
	fmt.Println("attempt to delete file", filePath)

	cmd := exec.Command("rm", filePath)
	out := bytes.Buffer{}
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("removed file", filePath, out.String())
}

// Helper function to watch the output of the commands being run
func copyAndCapture(w io.Writer, r io.Reader) ([]byte, error) {
	var out []byte
	buf := make([]byte, 1024, 1024)
	for {
		n, err := r.Read(buf[:])
		if n > 0 {
			d := buf[:n]
			out = append(out, d...)
			_, err := w.Write(d)
			if err != nil {
				return out, err
			}
		}
		if err != nil {
			// Read returns io.EOF at the end of file, which is not an error for us
			if err == io.EOF {
				err = nil
			}
			return out, err
		}
	}
}

// TestForApps will run version commands on all the needed applications
// if something doesn't run the program will end with an error
func TestForApps() {
	fmt.Println("----------")

	cmd := exec.Command("rsync", "--version")
	var out1, out2, out3 bytes.Buffer
	cmd.Stdout = &out1
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(out1.String())
	fmt.Println("----------")

	cmd = exec.Command("mediainfo", "--version")
	cmd.Stdout = &out2
	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(out2.String())
	fmt.Println("----------")

	cmd = exec.Command("ffmpeg", "-version")
	cmd.Stdout = &out3
	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(out3.String())
	fmt.Println("----------")
}
