FROM ubuntu:latest

RUN apt-get update -qq 
RUN apt-get -y install autoconf
RUN apt-get -y install automake 
RUN apt-get -y install build-essential 
RUN apt-get -y install cmake 
RUN apt-get -y install git-core 
RUN apt-get -y install libass-dev 
RUN apt-get -y install libfreetype6-dev 
RUN apt-get -y install libsdl2-dev 
RUN apt-get -y install libtool 
RUN apt-get -y install libva-dev 
RUN apt-get -y install libvdpau-dev 
RUN apt-get -y install libvorbis-dev 
RUN apt-get -y install libxcb1-dev 
RUN apt-get -y install libxcb-shm0-dev 
RUN apt-get -y install libxcb-xfixes0-dev 
RUN apt-get -y install pkg-config 
RUN apt-get -y install texinfo 
RUN apt-get -y install wget 
RUN apt-get -y install zlib1g-dev

RUN mkdir -p ~/ffmpeg_sources ~/bin
RUN apt-get -y install nasm
RUN apt-get -y install yasm
RUN apt-get -y install libx264-dev
RUN apt-get -y install libnuma-dev
RUN apt-get -y install libvpx-dev
RUN apt-get -y install libfdk-aac-dev
RUN apt-get -y install libmp3lame-dev
RUN apt-get -y install libopus-dev
RUN apt-get -y install mercurial
RUN apt-get -y install cmake-curses-gui

### x265 https://bitbucket.org/multicoreware/x265/wiki/Home
RUN cd ~/ffmpeg_sources && \
    hg clone https://bitbucket.org/multicoreware/x265 && \
    cd x265/build/linux && \
    PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED=off ../../source && \
    PATH="$HOME/bin:$PATH" make && \
    make -j8 install

RUN cd ~/ffmpeg_sources && \
wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && \
tar xjvf ffmpeg-snapshot.tar.bz2 && \
cd ffmpeg && \
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
  --prefix="$HOME/ffmpeg_build" \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" \
  --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
  --extra-libs="-lpthread -lm" \
  --bindir="$HOME/bin" \
  --enable-gpl \
  --enable-libass \
  --enable-libfdk-aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-nonfree && \
PATH="$HOME/bin:$PATH" make && \
make -j8 install && \
hash -r
