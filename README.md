# Rencode

This is a command line app, that takes a flag of -path and -preset

## Flags
* path
    * this is required, the file or folder (of files) to re-encode to h265

* preset
    * The speed (which effects file size and quality) of the encoded file
    * Valid values
        * ultrafast superfast veryfast faster fast medium slow slower veryslow placebo
        * default value is medium

## Required applications (must be available in your PATH)
  * ffmpeg
  * mediainfo (command line)
  * rsync (mac and linux only for now) need to look into windows options

## Example
```bash
 
./rencode -path /path/to/folder -preset fast
```
